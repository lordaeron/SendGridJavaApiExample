package com.azurian.enviarcorreo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import org.json.JSONObject;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGrid.Email;
import com.sendgrid.SendGridException;
import com.sendgrid.smtpapi.SMTPAPI;

/*
 * Dentro del proyecto esta la carpeta libs en donde se agregaron
 * los .jar para enviar correos basta con agregar el jar "sendgrid-java.jar"
 * m�s info https://github.com/sendgrid/sendgrid-java
 */

public class Main {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		// Key para acceder al env�o de correos v�a API
		// NOTA: Esta key es temporal para el per�odo de desarrollo y prueba
		String API_KEY = "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";
		SendGrid sendgrid = new SendGrid(API_KEY);
		Email email = new Email();

		// para esta demo se puede utilizar algun pdf por ejemplo para enviarlo como adjunto
		//File miXml = new File("C:\\Users\\crojas\\Google Drive\\workspace\\EnviarCorreo\\src\\fact.xml");
		File miPdf = new File("C:\\Users\\crojas\\Google Drive\\workspace\\EnviarCorreo\\src\\basico.pdf");
		
		// se prepara el objeto map con campos personalizados
		// Estos campos se definir�n en los requerimientos para la implementaci�n
		JSONObject map = new JSONObject();
		map.put("empresa", "chilquinta");
		map.put("rut_receptor", "12345678-9");
		map.put("rut_emisor", "12345678-9");
		map.put("tipo_envio", "notificacion");
		map.put("tipo_dte", "boleta");
		map.put("numero_folio", "10000");
		map.put("resolucion_receptor", "");
		map.put("resolucion_emisor", "");
		map.put("monto", "100000");
		map.put("fecha_emision", new Date());
		map.put("fecha_recepcion", "");
		map.put("estado_documento", "");
		map.put("tipo_operacion", "venta");
		map.put("tipo_receptor", "cliente");
		
		// preparaci�n de parametros propios de un email
		email.addTo("crojas@azurian.com", "Christian Rojas");
		//email.addToName("Christian Rojas");
		email.setFrom("facturacion@chilquinta.com");
		email.setFromName("Facturaci�n Chilquinta");
		email.setSubject("Boleta electr�nica");
		email.setHtml("<div align='center'><h1>Hola</h1><p>Saludos desde sendgrid \n Adios!.</p></div>");
		
		// Archivo adjunto
		email.addAttachment(miPdf.getName(), miPdf);
		
		// se agrega el objeto map de campos personalizados del correo
		email.getSMTPAPI().setUniqueArgs(map);
		
		try {
			SendGrid.Response response = sendgrid.send(email);
			System.out.println(response.getCode());
			System.out.println(response.getMessage());
			System.out.println(response.getStatus());
		} catch (SendGridException e) {
			e.printStackTrace();
		}
		
	}

}
